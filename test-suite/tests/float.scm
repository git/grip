;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(define-module (tests float)
  #:use-module (ice-9 receive)
  #:use-module (oop goops)
  #:use-module (unit-test)
  #:use-module (grip float))


(define-class <grip-tests-float> (<test-case>))


(define-method (test-float-round (self <grip-tests-float>))
  (assert-true (float=? (float-round 1.12345) 1.12))
  (assert-true (float=? (float-round 1.12345 2) 1.12))
  (assert-true (float=? (float-round 1.12345 3) 1.123)))

(define-method (test-float-member (self <grip-tests-float>))
  (let ((vals '(1.12 1.0e-4)))
    (assert-true (= (float-member 1.12 vals) 0))
    (assert-true (= (float-member 1.0e-4 vals) 1))
    (assert-false (float-member 1.13 vals))
    (assert-false (float-member 1.0e-3 vals))))


(exit-with-summary (run-all-defined-test-cases))
