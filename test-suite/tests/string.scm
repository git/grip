;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(define-module (tests string)
  #:use-module (ice-9 receive)
  #:use-module (oop goops)
  #:use-module (unit-test)
  #:use-module (grip string))


(define-class <grip-tests-string> (<test-case>))


(define-method (test-string-replace-all (self <grip-tests-string>))
  (assert-true (string=? (string-replace-all "aa.bb.aa.cc.dd" "aa" "cc")
                         "cc.bb.cc.cc.dd"))
  (assert-true (string=? (string-replace-all "aa.bb.aaa.cc.dd" "aa" "cc")
                         "cc.bb.cca.cc.dd"))
  (assert-true (string=? (string-replace-all "aa.bb.aaaa.cc.dd" "aa" "cc")
                         "cc.bb.cccc.cc.dd"))
  (assert-true (string=? (string-replace-all "aa.bb.aa.cc.dd" "aa" "cc"
                                             #:start 2)
                         "aa.bb.cc.cc.dd"))
  (assert-true (string=? (string-replace-all "aa.bb.aa.cc.dd" "aa" "cc"
                                             #:start 2 #:end 4)
                         "aa.bb.aa.cc.dd")))

(define-method (test-string-delete-all (self <grip-tests-string>))
  (assert-true (string=? (string-delete-all "aa.bb.cc.dd" "bb")
                         "aa..cc.dd"))
  (assert-true (string=? (string-delete-all "aa.bbb.cc.dd" "bb")
                         "aa.b.cc.dd"))
  (assert-true (string=? (string-delete-all "aa.bbbb.cc.dd" "bb")
                         "aa..cc.dd"))
  (assert-true (string=? (string-delete-all "aa.bbbb.cc.dd" "bb"
                                            #:start 5)
                         "aa.bb.cc.dd"))
  (assert-true (string=? (string-delete-all "aa.bbbb.cc.dd" "bb"
                                            #:start 6 #:end 8)
                         "aa.bbbb.cc.dd")))

(define-method (test-string-escape-sql (self <grip-tests-string>))
  (assert-true (string=? (string-escape-sql "aa'bb''cc'''dd")
                         "aa''bb''''cc''''''dd")))

(define-method (test-string-escape-filename (self <grip-tests-string>))
  (assert-true (string=? (string-escape-filename "d <>|()&;#?*\\")
                         "d\\ \\<\\>\\|\\(\\)\\&\\;\\#\\?\\*\\\\"))
  (assert-true (string=? (string-escape-filename "\\d <>|()&;#?*\\")
                         "\\\\d\\ \\<\\>\\|\\(\\)\\&\\;\\#\\?\\*\\\\"))
  (assert-true (string=? (string-escape-filename "d <>|()&;#?*\\.png")
                         "d\\ \\<\\>\\|\\(\\)\\&\\;\\#\\?\\*\\\\.png")))

(define-method (test-string-tokens (self <grip-tests-string>))
  (assert-equal (string-tokens "d-p_o" #\- #\_)
                '("d" "p" "o")))

(define-method (test-string-tokens (self <grip-tests-string>))
  (assert-equal (string-tokens "d-p_o" #\- #\_)
                '("d" "p" "o")))

(define-method (test-string-contains-ixs (self <grip-tests-string>))
  (assert-true (receive (l-index r-index s1-length)
                   (string-contains-ixs "debian" "ia")
                 (and (= l-index 3)
                      (= r-index 5)
                      (= s1-length 2))))
  (assert-true (receive (l-index r-index s1-length)
                   (string-contains-ixs "debian" "an")
                 (and (= l-index 4)
                      (= r-index 6)
                      (= s1-length 2)))))

(define-method (test-string-span (self <grip-tests-string>))
  (assert-exception (string-span "Right" "foreground"))
  (assert-true (string=? (string-span "Right" "foreground" "#98fb98")
                         "<span foreground=\"#98fb98\">Right</span>"))
  (assert-true (string=? (string-span "david" "colour" "blue" "size" 12)
                         "<span colour=\"blue\" size=\"12\">david</span>")))


(exit-with-summary (run-all-defined-test-cases))
