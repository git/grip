;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(define-module (tests utils)
  #:use-module (oop goops)
  #:use-module (unit-test)
  #:use-module (grip utils))


(define-class <grip-tests-utils> (<test-case>))


(define-method (test-dimfi (self <grip-tests-utils>))
  (assert-true (= (dimfi 1 2) 2)))

(define-method (test-and-l (self <grip-tests-utils>))
  (assert-exception (and-l '()))
  (assert-true (and-l '(a b c d)))
  (assert-false (and-l '(a #f c d)))
  (assert-false (and-l '(a #f c #f))))

(define-method (test-swap! (self <grip-tests-utils>))
  (let ((a 1)
        (b 2))
    (swap! a b)
    (assert-true (and (= a 2) (= b 1)))))


(exit-with-summary (run-all-defined-test-cases))
