;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(define-module (tests xft)
  #:use-module (ice-9 receive)
  #:use-module (oop goops)
  #:use-module (unit-test)
  #:use-module (grip xft))


(define-class <grip-tests-xft> (<test-case>))


(define-method (test-xft-store (self <grip-tests-xft>))
  (let ((store (xft-store)))
    (assert-true store)
    (assert-true (eq? store (xft-store)))
    (assert-false (eq? store (xft-store #:reload #t)))
    (assert (xft-store #:default-dpi 72 #:reload #t))
    (assert (get store 'Xft.antialias))
    (assert (get store 'Xft.autohint))
    (assert (get store 'Xft.dpi))
    (assert (get store 'Xft.hinting))
    (assert (get store 'Xft.hintstyle))
    (assert (get store 'Xft.rgba))
    (assert (get store 'scale-factor))
    (assert (get store 'apply-scale-factor?))))


(exit-with-summary (run-all-defined-test-cases))
