;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(define-module (tests list)
  #:use-module (ice-9 receive)
  #:use-module (oop goops)
  #:use-module (unit-test)
  #:use-module (grip list))


(define-class <grip-tests-list> (<test-case>))


(define-method (test-list-pos (self <grip-tests-list>))
  (assert-true (= (list-pos 'foo '(foo bar baz)) 0))
  (assert-true (= (list-pos '2 '(1 2 3) =) 1))
  (assert-false (list-pos 'baz '(foo bar))))

(define-method (test-list-insert (self <grip-tests-list>))
  (assert-equal (list-insert 'foo '(bar baz) -1)
                '(foo bar baz))
  (assert-equal (list-insert 'foo '(bar baz) 0)
                '(foo bar baz))
  (assert-equal (list-insert 'bar '(foo baz) 1)
                '(foo bar baz))
  (assert-equal (list-insert 'baz '(foo bar) 2)
                '(foo bar baz))
  (assert-equal (list-insert 'baz '(foo bar) 3)
                '(foo bar baz)))

(define-method (test-list-replace-all (self <grip-tests-list>))
  (assert-equal (list-replace-all 'fuu 'bar '(foo bar baz))
                '(foo bar baz))
  (assert-equal (list-replace-all 'foo 'bar '(foo bar baz))
                '(bar bar baz))
  (assert-equal (list-replace-all 'foo 'bar '(foo bar foo baz))
                '(bar bar bar baz))
  (assert-equal (list-replace-all 1 2 '(1 2 3 1 4) =)
                '(2 2 3 2 4)))

(define-method (test-list-interleave (self <grip-tests-list>))
  (assert-equal (interleave '(1 2 3) '(4 5 6))
                '(1 4 2 5 3 6))
  (assert-equal (interleave '(1 2 3) '(4 5))
                '(1 4 2 5))
  (assert-equal (interleave '(1 2) '(4 5 6))
                '(1 4 2 5))
  (assert-equal (interleave '(1 2 3) '(4 5 6) '(foo bar baz))
                '(1 4 foo 2 5 bar 3 6 baz))
  (assert-equal (interleave '(1 2 3) '(4 5 6) '(foo bar))
                '(1 4 foo 2 5 bar))
  (assert-equal (interleave '(1 2 3) '(4 5) '(foo bar baz))
                '(1 4 foo 2 5 bar))
  (assert-equal (interleave '(1 2) '(4 5 6) '(foo bar baz))
                '(1 4 foo 2 5 bar)))

(define-method (test-list-flatten (self <grip-tests-list>))
  (assert-equal (flatten '(1 2 (3) (4 (5 (6 (7 8) 9) (10 11))) 12))
                '(1 2 3 4 5 6 7 8 9 10 11 12)))


(exit-with-summary (run-all-defined-test-cases))
