;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(define-module (tests optargs)
  #:use-module (ice-9 receive)
  #:use-module (oop goops)
  #:use-module (unit-test)
  #:use-module (grip optargs))


(define-class <grip-tests-optargs> (<test-case>))


(define-method (test-split-keyword-args (self <grip-tests-optargs>))
  (receive (kw rest)
      (split-keyword-args '(#:a #:b) '(#:a 1 #:b 2 3 #:c 4))
    (assert-true (and (equal? kw '(#:a 1 #:b 2))
                      (equal? rest '(3 #:c 4))))))

(define-method (test-strip-keyword-args (self <grip-tests-optargs>))
  (assert-true (equal? (strip-keyword-args '(#:a #:b) '(#:a 1 #:b 2 3 #:c 4))
                       '(3 #:c 4))))


(exit-with-summary (run-all-defined-test-cases))
