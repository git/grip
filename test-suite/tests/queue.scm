;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(define-module (tests queue)
  #:use-module (ice-9 receive)
  #:use-module (oop goops)
  #:use-module (unit-test)
  #:use-module (grip queue))


(define-class <grip-tests-queue> (<test-case>))


(define-method (test-queue-push (self <grip-tests-queue>))
  (assert-equal (push 'a '(b c)) '(a b c)))

(define-method (test-queue-push! (self <grip-tests-queue>))
  (let ((queue '(b c)))
    (push! 'a queue)
    (assert-equal queue '(a b c))))

(define-method (test-queue-push* (self <grip-tests-queue>))
  (assert-equal (push* 'a 'b '(c d)) '(a b c d)))

(define-method (test-queue-push*! (self <grip-tests-queue>))
  (let ((queue '(c d)))
    (push*! 'a 'b queue)
    (assert-equal queue '(a b c d))))

(define-method (test-queue-pop (self <grip-tests-queue>))
  (assert-equal (pop '(a b c)) '(b c)))

(define-method (test-queue-pop! (self <grip-tests-queue>))
  (let ((queue '(a b c)))
    (pop! queue)
    (assert-equal queue '(b c))))

(define-method (test-queue-pop* (self <grip-tests-queue>))
  (assert-equal (pop* '(a b c d) 2) '(c d)))

(define-method (test-queue-pop*! (self <grip-tests-queue>))
  (let ((queue '(a b c d)))
    (pop*! queue 2)
    (assert-equal queue '(c d))))


(exit-with-summary (run-all-defined-test-cases))

  

#!

(define l1 '(b a))
(push 'c l1)
l1
(push! 'c l1)
l1

(define l2 '(c d))
(push* 'a 'b l2)
l2
(push*! 'a 'b l2)
l2

(define l1 '(b a))
(pop l1)
l1
(pop! l1)
l1

(define l2 '(c d))
(pop* l2 2)
l2
(pop* l2 3)
l2

(pop*! l2 2)
l2
(pop*! l2 3)
l2

!#
