;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(define-module (tests store)
  #:use-module (ice-9 receive)
  #:use-module (oop goops)
  #:use-module (unit-test)
  #:use-module (grip store))


(define-class <grip-tests-store> (<test-case>))


(define-method (test-make (self <grip-tests-store>))
  (assert (make <store>))
  (assert-true (eq? (ref (make <store>
                           #:store '((foo . bar))) 'foo)
                    'bar))
  (assert-exception (make <store>
                      #:store '((foo . bar) baz)))
  (assert-exception (make <store>
                      #:store '((foo . bar) (baz 2))))
  (assert-exception (make <store>
                      #:store '((foo . bar) (foo . baz))))
  (assert-exception (make <store>
                      #:store '((2 . bar)))))

(define-method (test-get (self <grip-tests-store>))
  (let ((store (make <store>
                 #:store '((foo . bar)))))
    (assert-true (equal? (get store 'foo) '(foo . bar)))
    (assert-false (get store 'red))))

(define-method (test-ref (self <grip-tests-store>))
  (let ((store (make <store>
                 #:store '((foo . bar)))))
    (assert-true (eq? (ref store 'foo) 'bar))
    (assert-false (ref store 'red))))

(define-method (test-set-! (self <grip-tests-store>))
  (let ((store (make <store>)))
    (assert-true (equal? (set-! store 'foo 'bar) '((foo . bar))))
    (set-! store 'baz 2)
    (assert-true (and (eq? (ref store 'foo) 'bar)
                      (= (ref store 'baz) 2)))))

(define-method (test-remove! (self <grip-tests-store>))
  (let ((store (make <store>
                 #:store '((foo . bar)))))
    (assert-true (equal? (remove! store 'baz) '((foo . bar))))
    (assert-true (null? (remove! store 'foo)))))

(define-method (test-inventory (self <grip-tests-store>))
  (let ((store (make <store>
                 #:store '((foo . bar)))))
    (assert-true (equal? (inventory store) '((foo . bar))))))

(define-method (test-init! (self <grip-tests-store>))
  (let ((store (make <store>)))
    (assert-true (eq? (init! store '((foo . bar))) store))
    (assert-true (null? (inventory (init! store '()))))
    (assert-exception (init! store '((1 . bar))))
    (assert (init! store '((1 . bar)) #:no-checks #t))))

(define-method (test-save (self <grip-tests-store>))
  (let ((store (make <store>
                 #:store '((foo . bar)))))
    (assert (save store "/tmp/grip-test-suite-store"))))

(define-method (test-load! (self <grip-tests-store>))
  (let ((store (make <store>)))
    (assert (load! store "/tmp/grip-test-suite-store"))
    (assert-true (equal? (inventory store) '((foo . bar))))))

(define-method (test-store-invntory? (self <grip-tests-store>))
  (receive (pass? comment pair)
      (store-inventory? '((foo . bar)))
    (assert-true (and pass?
                      (eq? comment 'well-formed))))
  (receive (pass? comment pair)
      (store-inventory? '((foo . bar) baz))
    (assert-true (and (not pass?)
                      (eq? comment 'not-a-pair))))
  (receive (pass? comment pair)
      (store-inventory? '((foo . bar) (baz 2)))
    (assert-true (and (not pass?)
                      (eq? comment 'not-a-pair))))
  (receive (pass? comment pair)
      (store-inventory? '((foo . bar) (foo . baz)))
    (assert-true (and (not pass?)
                      (eq? comment 'duplicate-key))))
  (receive (pass? comment pair)
      (store-inventory? '((2 . bar)))
    (assert-true (and (not pass?)
                      (eq? comment 'wrong-key-type)))))


(exit-with-summary (run-all-defined-test-cases))
