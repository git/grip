;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2011 - 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;; string-replace-all inspired by the similar procedure in guix
;;   http://git.savannah.gnu.org/cgit/guix.git/tree/guix/utils.scm

;;; Code:


(define-module (grip string)
  #:use-module (ice-9 match)
  #:use-module (ice-9 receive)
  #:use-module (grip optargs)
  #:use-module (grip utils)

  #:export (string-replace-all
	    string-delete-all
            string-escape
            string-escape-sql
            %filename-reserved-chars
            string-escape-filename
	    string-tokens
	    string-contains-ixs
            string-contains-ci-ixs
	    string-span
	    string-read))


(define* (string-replace-all str s1 s2 #:key
                             (start 0)
                             (end (string-length str)))
  "Replace all occurrences of S1 in the START--END range of STR by S2."
  (match (string-length s1)
    (0
     (error "string-replace-all: empty s1"))
    (s1-length
     (let loop ((start start)
                (tokens (list (substring str 0 start))))
       (match (string-contains str s1 start end)
         (#f
          (string-concatenate-reverse
           (cons (substring str start) tokens)))
         (index
          (loop (+ index s1-length)
                (cons* s2
                       (substring str start index)
                       tokens))))))))

(define* (string-delete-all str s1  #:key
                            (start 0)
                            (end (string-length str)))
  (string-replace-all str s1 "" #:start start #:end end))

(define (string-escape str . chars)
  (let loop ((result str)
             (esc chars))
    (if (null? esc)
        result
        (let* ((s1 (string (car esc)))
               (s2 (string-append "\\" s1)))
          (loop (string-replace-all result s1 s2)
                (cdr esc))))))

(define (string-escape-sql str)
  ;; ' -> ''
  (string-replace-all str "'" "''"))

(define %filename-reserved-chars
  ;; it is _mandatory_ that the first character to be escaped be #\,
  ;; since it is the escape character itself - otherwise, recursive
  ;; results would be (re)escaped ...
  '(#\\ #\space #\< #\> #\| #\( #\) #\& #\; #\# #\? #\*))

(define* (string-escape-filename str #:optional (chars %filename-reserved-chars))
  (apply string-escape str chars))

(define* (string-tokens str #:key
                        (start 0)
                        (end (string-length str))
                        . chars)
  (let ((chars (strip-keyword-args '(#:start #:end) chars)))
    (string-tokenize str
                     (char-set-complement (apply char-set chars))
                     start end)))

(define* (string-contains-ixs s1 s2 #:key
                              (start1 0)
                              (end1 (string-length s1))
                              (start2 0)
                              (end2 (string-length s2)))
  (let* ((l-index (string-contains s1 s2 start1 end1 start2 end2))
	 (s2-length (string-length s2))
	 (r-index (and l-index (+ l-index s2-length))))
    (values l-index r-index s2-length)))

(define* (string-contains-ci-ixs s1 s2 #:key
                                 (start1 0)
                                 (end1 (string-length s1))
                                 (start2 0)
                                 (end2 (string-length s2)))
  (let* ((l-index (string-contains-ci s1 s2 start1 end1 start2 end2))
	 (s2-length (string-length s2))
	 (r-index (and l-index (+ l-index s2-length))))
    (values l-index r-index s2-length)))

(define (make-span-attrs-string attrs)
  (if (odd? (length attrs))
      (error "string-pan: wrong number of attributes.")
      (let loop ((plist attrs)
                 (markup-string ""))
        (match plist
          ((prop val . rests)
           (loop rests
                 (string-append markup-string
                                prop
                                "=\""
                                (if (string? val)
                                    val
                                    (number->string val))
                                (if (null? rests) "\"" "\" "))))
          (()
           (string-append markup-string ">"))))))

(define (string-span str . attrs)
  (string-append "<span "
                 (make-span-attrs-string attrs)
                 str
                 "</span>"))

(define (string-read str)
  (with-input-from-string str read))
