;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2011 - 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(define-module (grip float)
  #:use-module ((rnrs arithmetic flonums) :version (6))
  #:use-module (ice-9 match)
  ;; #:use-module (grip utils)
  ;; #:use-module (cv support libgrip)

  #:export (float-zero?
	    float=?
	    float<?
	    float<=?
	    float>?
	    float>=?
	    float-round
	    float-member
            #;float->int))


;;;
;;; Variables
;;;


;;;
;;; Procedures
;;;

(define float-zero? flzero?)

(define (float=? . floats)
  (apply fl=? floats))

(define (float<? . floats)
  (apply fl<? floats))

(define (float<=? . floats)
  (apply fl<=? floats))

(define (float>? . floats)
  (apply fl>? floats))

(define (float>=? . floats)
  (apply fl>=? floats))

(define (float-round float . dec)
  (let ((m (match dec
	     (() 100)
	     ((n-dec) (expt 10 n-dec)))))
    (/ (round (* m float)) m)))

(define (float-member f vals)
  (let loop ((i 0)
             (vals vals))
    (match vals
      (() #f)
      ((f-val . rests)
       (if (float=? f-val f)
           i
           (loop (+ i 1)
                 rests))))))


;;;
;;; From libgrip
;;;

#;(define float->int float-to-int-c)
