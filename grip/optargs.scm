;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2011 - 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;; split-keyword-args has been rewritten, greatly inspired by the code
;; of the Guix procedure strip-keyword-arguments, which you may look at
;; here:

;; http://git.savannah.gnu.org/cgit/guix.git/tree/guix/utils.scm

;;; Code:


(define-module (grip optargs)
  #:use-module (ice-9 match)
  #:use-module (ice-9 receive)
  
  #:export (split-keyword-args
            strip-keyword-args))


(define (split-keyword-args keywords args)
  (let loop ((args args)
             (split-kw '())
             (split-rest '()))
    (match args
      (()
       (values (reverse! split-kw) (reverse! split-rest)))
      (((? keyword? kw) arg . rest)
       (loop rest
             (if (memq kw keywords)
                 (cons* arg kw split-kw)
                 split-kw)
             (if (memq kw keywords)
                 split-rest
                 (cons* arg kw split-rest))))
      ((head . tail)
       (loop tail split-kw (cons head split-rest))))))

(define (strip-keyword-args keywords args)
  (receive (split-kw split-rest)
      (split-keyword-args keywords args)
    split-rest))
