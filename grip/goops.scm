;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2011 - 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;; define-method* has been written by Mark H Weaver, I just changed
;; let-values -> receive, which I find (a lot) more readable. I can't
;; point to 'an original definition location' though, because Mark
;; pasted his definition while chating on #guile, a few years ago.  I
;; don't think Mark ever published this code anywhere, but I could be
;; wrong, if that is the case, please let me know, I'd be happy to add
;; the link here.

;;; Code:


(define-module (grip goops)
  #:use-module (ice-9 receive)
  #:use-module (srfi srfi-1)
  #:use-module (oop goops)
  #:use-module (oop goops describe)
  #:use-module (grip module)

  #:duplicates (merge-generics
		replace
		warn-override-core
		warn
		last)

  #:export (define-method*))


(g-export class-direct-virtual-slots
	  class-virtual-slots
	  describe)


(eval-when (expand load eval)
  (re-export-public-interface (oop goops)))


(define-syntax define-method*
  (lambda (x)
    (syntax-case x ()
      ((_ (generic arg-spec ... . tail) body ...)
       (receive (required-arg-specs other-arg-specs)
           (break (compose keyword? syntax->datum)
                  #'(arg-spec ...))
         #`(define-method (generic #,@required-arg-specs . rest)
             (apply (lambda* (#,@other-arg-specs . tail)
                      body ...)
                    rest)))))))

(define-method (class-direct-virtual-slots (class <class>))
  (filter-map (lambda (slot-definition)
		(and (eq? (slot-definition-allocation slot-definition)
			  #:virtual)
		     slot-definition))
      (class-direct-slots class)))

(define-method (class-virtual-slots (class <class>))
  (filter-map (lambda (slot-definition)
		(and (eq? (slot-definition-allocation slot-definition)
			  #:virtual)
		     slot-definition))
      (class-slots class)))
