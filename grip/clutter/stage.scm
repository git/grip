;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2011 - 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;       <- stands for ->
;; clue                   clutter example
;; clues                  clutter examples set
;; clus                   clutter support

;; This needs review

;;; Code:


(define-module (grip clutter stage)
  #:use-module (ice-9 match)
  #:use-module (ice-9 receive)
  #:use-module (ice-9 optargs)
  #:use-module (srfi srfi-1)
  #:use-module (oop goops)
  #:use-module (gnome-2)
  #:use-module (gnome gobject)
  #:use-module (gnome glib)
  #:use-module (gnome clutter)
  #:use-module (grip module)
  #:use-module (grip goops)
  #:use-module (grip optargs)
  #:use-module (grip utils)
  #:use-module (grip clutter utils)
  #:use-module (grip clutter color)
  #:use-module (grip clutter globals)

  #:duplicates (merge-generics
		replace
		warn-override-core
		warn
		last)

  #:export (<clus-stage>
	    
	    clus-stage-get-item-on-hold
	    clus-stage-set-item-on-hold))


(g-export !color)


(define clus-stage-get-item-on-hold #f)
(define clus-stage-set-item-on-hold #f)


(let ((toolbar-item-on-hold #f))
  (set! clus-stage-get-item-on-hold
	(lambda () toolbar-item-on-hold))
  (set! clus-stage-set-item-on-hold
	(lambda (val) (set! toolbar-item-on-hold val))))


;;;
;;; Stage
;;;

(define-class <clus-stage> (<clutter-stage>)
  (color #:accessor !color
	  #:init-keyword #:color
	  #:allocation #:virtual
	  #:slot-ref (lambda (obj)
		       (get-background-color obj))
	  #:slot-set! (lambda (obj value)
			(set-background-color obj value))))

(define-method (initialize (self <clus-stage>) initargs)
  ;; (dimfi "initialize <clus-stage>" initargs)
  (receive (virtual-kw rest)
      (split-keyword-args (map slot-definition-init-keyword
			    (class-direct-virtual-slots <clus-stage>))
                          initargs)
    (if (null? virtual-kw)
	(next-method self rest)
	(begin
	  (next-method self rest)
	  (let-keywords virtual-kw #t
			((color #f))
			(when color (set! (!color self) color)))))
    (connect self
	     'delete-event
	     (lambda (. args)
	       (clutter-main-quit)
	       #t)) ;; stops the event to be propagated
    #;(connect self
	     'button-press-event
	     (lambda (i e)
	       (dimfi "press on stage")
	       #f)) ;; do not stop the event to be propagated
    (connect self
	     'button-release-event
	     (lambda (actor event)
	       (when (clus-stage-get-item-on-hold)
		 (clus-stage-set-item-on-hold #f))
	       #f)))) ;; do not stop the event from being propagated
