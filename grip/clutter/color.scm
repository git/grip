;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2011 - 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;       <- stands for ->
;; clue                   clutter example
;; clues                  clutter examples set
;; clus                   clutter support

;; this needs review, and a better integration with clutter.  it's not
;; even using <clutter-color>, but i needed something quick.

;;; Code:


(define-module (grip clutter color)
  #:use-module (ice-9 regex)
  #:use-module (srfi srfi-1)
  #:use-module (oop goops)
  #:use-module (gnome-2)
  #:use-module (gnome gobject)
  #:use-module (gnome clutter)
  #:use-module (grip module)
  #:use-module (grip goops)
  #:use-module (grip utils)
  #:use-module (grip string)
  #:use-module (grip list)

  #:duplicates (merge-generics
		replace
		warn-override-core
		warn
		last)

  #:export (get-darker-color
	    get-lighter-color
	    get-rgba-r get-rgba-g get-rgba-b get-rgba-a
	    get-rgba-values

	    xcolor?
	    xcolor->int
	    xcolor->rgb
	    xcolor->rgba

	    %tango-butter
	    %tango-butter-light
	    %tango-butter-dark

	    %tango-orange
	    %tango-orange-light
	    %tango-orange-dark

	    %tango-chocolate
	    %tango-chocolate-light
	    %tango-chocolate-dark

	    %tango-chameleon
	    %tango-chameleon-light
	    %tango-chameleon-dark

	    %tango-sky-blue
	    %tango-sky-blue-light
	    %tango-sky-blue-dark

	    %tango-plum
	    %tango-plum-light
	    %tango-plum-dark

	    %tango-scarlet-red
	    %tango-scarlet-red-light
	    %tango-scarlet-red-dark

	    %tango-aluminium-1
	    %tango-aluminium-2
	    %tango-aluminium-3
	    %tango-aluminium-4
	    %tango-aluminium-5
	    %tango-aluminium-6))


(g-export get-color)


(define-method* (get-color (color <string>)
                           #:key (alpha 255) (fallback "Chocolate"))
  (let ((color (or (clutter-color-from-string color)
                   (begin
                     (display (string-append "WARNING: Undefined color " color
                                             ", using " fallback " instead\n"))
                     (clutter-color-from-string fallback)))))
    (if (= alpha 255)
        color
        (append (drop-right color 1) (list alpha)))))

(define-method (get-color (color <list>))
  ;; This method is sometimes/can be called on colors that are clutter
  ;; colors already. In this case, we just return the argument.
  color)

(define* (get-darker-color color #:key (ratio 0.4))
  (let ((channels (if (string? color) (get-color color) color)))
    ;; nothing on the alpha channel
    (append (map (lambda (channel)
		   (- channel (inexact->exact (round (* channel ratio)))))
	      (drop-right channels 1))
	    (last-pair channels))))

(define* (get-lighter-color color #:key (ratio 0.4))
  (let ((channels (if (string? color) (get-color color) color)))
    ;; nothing on the alpha channel
    (append (map (lambda (channel)
		   (+ channel (inexact->exact (round (* channel ratio)))))
	      (drop-right channels 1))
	    (last-pair channels))))

(define (get-channel-pos channel)
  (case channel
    ((red) 0)
    ((green) 1)
    ((blue) 2)
    ((alpha) 3)))

(define* (get-rgba-r color #:key (float #f))
  (let ((r (list-ref color (get-channel-pos 'red))))
    (if float (/ r 255) r)))

(define* (get-rgba-g color #:key (float #f))
  (let ((g (list-ref color (get-channel-pos 'green))))
    (if float (/ g 255) g)))

(define* (get-rgba-b color #:key (float #f))
  (let ((b (list-ref color (get-channel-pos 'blue))))
    (if float (/ b 255) b)))

(define* (get-rgba-a color #:key (float #f))
  (let ((a (list-ref color (get-channel-pos 'alpha))))
    (if float (/ a 255) a)))

(define* (get-rgba-values color #:key (float #f))
  (values (get-rgba-r color #:float float)
	  (get-rgba-g color #:float float)
	  (get-rgba-b color #:float float)
	  (get-rgba-a color #:float float)))


;;;
;;; Xcolor [hex code] procs
;;;

;; Let's keep these for the record, but I finally discovered that
;; clutter-color-from-string does the same job [and more]! Oh well ...

(define %xcolor-regexp-1
  (make-regexp "^#[0-9a-fA-F]{3}$"))
(define %xcolor-regexp-2
  (make-regexp "^[0-9a-fA-F]{3}$"))

(define %xcolor-regexp-3
  (make-regexp "^#[0-9a-fA-F]{6}$"))
(define %xcolor-regexp-4
  (make-regexp "^[0-9a-fA-F]{6}$"))

(define (xcolor? xcolor)
  ;; ex: "#fff", "#ffffff"
  (cond ((integer? xcolor)
	 xcolor)
	((string? xcolor)
	 (let* ((xlist (if (char=? (string-ref xcolor 0) #\#)
			   (drop (string->list xcolor) 1)
			   (string->list xcolor)))
		(xlist-6 (cond ((or (regexp-exec %xcolor-regexp-1 xcolor)
				    (regexp-exec %xcolor-regexp-2 xcolor))
				(interleave xlist xlist))
			       ((or (regexp-exec %xcolor-regexp-3 xcolor)
				    (regexp-exec %xcolor-regexp-4 xcolor))
				xlist)
			       (else
				#f))))
	   (and xlist-6
		(string-read (string-append "#x"
                                            (list->string xlist-6))))))
	(else
	 #f)))

(define (xcolor->int xcolor channel)
  "Return the integer value of an 8-bit color channel for XCOLOR."
  (let* ((xcolor (xcolor? xcolor))
	 (offset (case channel
		   ((alpha) 24)
		   ((red) 16)
		   ((green) 8)
		   ((blue) 0)))
	 (mask (ash #xff offset)))
    (and xcolor
	 (ash (logand mask xcolor)
	      (- offset)))))

(define (xcolor->rgb xcolor)
  "Returns a list of red, green and blue integer values for XCOLOR."
  (let ((xcolor (if (integer? xcolor)
		     xcolor
		     (xcolor? xcolor))))
    (and xcolor
	 (list (xcolor->int xcolor 'red)
	       (xcolor->int xcolor 'green)
	       (xcolor->int xcolor 'blue)))))

(define* (xcolor->rgba xcolor #:key (alpha 255))
  "Returns a list of red, green, blue and alpha integer values for
XCOLOR. The default value for the optional alpha channel keyword is 255."
  (append (xcolor->rgb xcolor)
	  (list alpha)))


;;;
;;; Tango color pallete
;;;   http://tango.freedesktop.org
;;;

(define %tango-butter (xcolor->rgba "#edd400"))
(define %tango-butter-light (xcolor->rgba "#fce94f"))
(define %tango-butter-dark (xcolor->rgba "#c4a000"))

(define %tango-orange (xcolor->rgba "#f57900"))
(define %tango-orange-light (xcolor->rgba "#fcaf3e"))
(define %tango-orange-dark (xcolor->rgba "#ce5c00"))

(define %tango-chocolate (xcolor->rgba "#c17d11"))
(define %tango-chocolate-light (xcolor->rgba "#e9b96e"))
(define %tango-chocolate-dark (xcolor->rgba "#8f5902"))

(define %tango-chameleon (xcolor->rgba "#73d216"))
(define %tango-chameleon-light (xcolor->rgba "#8ae234"))
(define %tango-chameleon-dark (xcolor->rgba "#4e9a06"))

(define %tango-sky-blue (xcolor->rgba "#3465a4"))
(define %tango-sky-blue-light (xcolor->rgba "#729fcf"))
(define %tango-sky-blue-dark (xcolor->rgba "#204a87"))

(define %tango-plum (xcolor->rgba "#75507b"))
(define %tango-plum-light (xcolor->rgba "#ad7fa8"))
(define %tango-plum-dark (xcolor->rgba "#5c3566"))

(define %tango-scarlet-red (xcolor->rgba "#cc0000"))
(define %tango-scarlet-red-light (xcolor->rgba "#ef2929"))
(define %tango-scarlet-red-dark (xcolor->rgba "#a40000"))

(define %tango-aluminium-1 (xcolor->rgba "#eeeeec"))
(define %tango-aluminium-2 (xcolor->rgba "#d3d7cf"))
(define %tango-aluminium-3 (xcolor->rgba "#babdb6"))
(define %tango-aluminium-4 (xcolor->rgba "#888a85"))
(define %tango-aluminium-5 (xcolor->rgba "#555753"))
(define %tango-aluminium-6 (xcolor->rgba "#2e3436"))
