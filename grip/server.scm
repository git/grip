;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2011 - 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(define-module (grip server)
  #:use-module (ice-9 receive)
  #:use-module (system repl server)

  #:export (run-server-any-port
            spawn-server-any-port))


(define (try-server-on-port proc port)
  (catch #t
    (lambda ()
      (proc (make-tcp-server-socket #:port port)))
    (lambda (key . parameters)
      #f)))

(define* (start-server-any-port proc #:key (port 1968))
  (let loop ((port port)
             (server (try-server-on-port proc port)))
    (if server
        (values port server)
        (let ((next (+ port 1)))
          (loop next
                (try-server-on-port proc next))))))

(define* (run-server-any-port #:key (port 1968))
  (receive (port thread)
      (start-server-any-port run-server #:port port)
    (values port thread)))

(define* (spawn-server-any-port #:key (port 1968))
  (receive (port thread)
      (start-server-any-port spawn-server #:port port)
    (values port thread)))


#!

;; couldn't make this work
(define (try-server-on-port proc host addr port)
  (catch #t
    (lambda ()
      (proc (make-tcp-server-socket #:host host #:addr addr #:port port)))
    (lambda (key . parameters)
      #f)))

(define* (start-server-any-port proc #:key
                                (host #f) (addr #f) (port 1968))
  (let loop ((port port)
             (server (try-server-on-port proc host addr port)))
    (if server
        (values port server)
        (let ((next (+ port 1)))
          (loop next
                (try-server-on-port proc host addr next))))))
!#
