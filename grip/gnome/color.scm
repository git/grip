;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2011 - 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(define-module (grip gnome color)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 rdelim)
  #:export (%palette
	    color-set
	    color-set-bg
	    color-set-border
	    color-set-fg
	    color-set-name
	    color-set-ids
	    color-set-names))


(define %palette #f)

(eval-when (expand load eval)
  (let ((whereami (dirname (search-path %load-path
					"grip/gnome/color.scm"))))
    (set! %palette
	 ;; (name . (background border foreground))
	  (call-with-input-file
	      (string-append whereami "/color-set")
	    read))))


(define (color-set id)
  (assq-ref %palette id))

(define (color-set-bg id)
  (list-ref (color-set id) 0))

(define (color-set-border id)
  (list-ref (color-set id) 1))

(define (color-set-fg id)
  (list-ref (color-set id) 2))

(define (color-set-name id)
  (list-ref (color-set id) 3))

(define (color-set-ids)
  (filter-map (lambda (set) (car set))
      %palette))

(define (color-set-names)
  (filter-map (lambda (entry) (color-set-name (car entry)))
      %palette))


;;;
;;; 
;;;


#!

(use-modules (grip gnome color))
(reload-module (resolve-module '(grip gnome color)))

%palette

(color-set 1)
(color-set-bg 1)
(color-set-border 1)
(color-set-fg 1)
(color-set-name 1)

(color-set-ids)
(color-set-names)


;; Gtk widget states

'((normal GTK_STATE_NORMAL 0)
  (active GTK_STATE_ACTIVE 1)
  (prelight GTK_STATE_PRELIGHT 2) 
  (selected GTK_STATE_SELECTED 3)
  (insensitive GTK_STATE_INSENSITIVE 4))

!#
