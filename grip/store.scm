;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(define-module (grip store)
  #:use-module (ice-9 match)
  #:use-module (ice-9 receive)
  #:use-module (ice-9 format)
  #:use-module (ice-9 optargs)
  #:use-module (oop goops)
  #:use-module (grip module)
  #:use-module (grip goops)
  #:use-module (grip optargs)
  #:use-module (grip utils)

  #:duplicates (merge-generics
		replace
		warn-override-core
		warn
		last)

  #:export (<store>
            store-inventory?))


(g-export get
          ref
          set-!
          remove!
          inventory
          init!
          load!
          save)


(define-class <store> ()
  (store #:accessor !store
         #:init-keyword #:store
         #:init-thunk list))

(define-method (initialize (self <store>) initargs)
  (receive (kw rest)
      (split-keyword-args (map slot-definition-init-keyword
			    (class-direct-slots <store>))
                          initargs)
    (if (null? kw)
	(next-method self initargs)
        (let-keywords kw #t
                      ((store #f))
          (if (not store)
              (next-method self initargs)
              (receive (pass? comment pair)
                  (store-inventory? store)
                (if pass?
                    (next-method self initargs)
                    (error "initialize failed: " comment))))))))

(define-method (get (self <store>) (key <symbol>))
  (assq key (!store self)))

(define-method (ref (self <store>) (key <symbol>))
  (assq-ref (!store self) key))

(define-method (set-! (self <store>) (key <symbol>) val)
  (let ((store (assq-set! (!store self) key val)))
    (set! (!store self) store)
    store))

(define-method (remove! (self <store>) (key <symbol>))
  (let ((store (assq-remove! (!store self) key)))
    (set! (!store self) store)
    store))

(define-method (inventory (self <store>))
  (!store self))

(define-method* (init! (self <store>) vals #:key (no-checks #f))
  (receive (pass? comment pair)
      (if no-checks
          (values #t 'well-formed #t)
          (store-inventory? vals))
    (if pass?
        (begin
          (set! (!store self) vals)
          self)
        (error "init! failed: " comment))))

(define-method* (load! (self <store>) filename #:key (no-checks #f))
  (let ((vals (call-with-input-file filename read)))
    (receive (pass? comment pair)
        (if no-checks
            (values #t 'well-formed #t)
            (store-inventory? vals))
      (if pass?
          (begin
            (set! (!store self) vals)
            self)
          (error "load! failed: " comment)))))

(define-method (save (self <store>) filename)
  (call-with-output-file filename
    (lambda (port)
      (format port "~S~%" (!store self)))))

(define (store-inventory? vals)
  (let loop ((store vals)
             (keys '()))
    (match store
      (()
       (values #t 'well-formed #t))
      ((pair . rests)
       (if (and (pair? pair)
                (not (list? pair)))
           (match pair
             ((key . value)
              (if (symbol? key)
                  (if (memq key keys)
                      (values #f 'duplicate-key pair)
                      (loop rests (cons key keys)))
                  (values #f 'wrong-key-type pair))))
           (values #f 'not-a-pair pair))))))
