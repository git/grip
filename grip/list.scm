;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2011 - 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(define-module (grip list)
  #:use-module (ice-9 match)
  #:use-module (ice-9 receive)
  #:use-module (srfi srfi-1)

  #:export (list-pos
	    list-insert
	    list-replace-all
            interleave
	    flatten
            ;; not part of Grip's API
	    first-level-pos-second-level-lookup))


;;;
;;; Part of Grip's API and Documented
;;;

(define* (list-pos item lst #:optional (pred eq?))
  (list-index (lambda (x) (pred x item))
	      lst))

(define (list-insert item lst pos)
  (let ((its-length (length lst)))
    (cond ((<= pos 0)
           (cons item lst))
          ((< pos its-length)
           (receive (split-a split-b)
               (split-at lst pos)
             (append split-a (list item) split-b)))
          (else
           (append lst (list item))))))

(define* (list-replace-all old new lst #:optional (pred eq?))
  (fold-right (lambda (item prev)
                (if (pred item old)
                    (cons new prev)
                    (cons item prev)))
              '()
              lst))

(define (interleave . lls)
  (concatenate (apply map list lls)))

(define (flatten lst)
 (let loop ((item lst)
            (result '()))
   (match item
     (()
      result)
     ((elt . rests)
      (loop elt
            (loop rests
                  result)))
     (else
      (cons item
            result)))))


;;;
;;; Not part of Grip's API and undocumented
;;;

(define (first-level-pos-second-level-lookup item lst pred)
  (let ((its-length (length lst)))
    (catch 'exit
	   (lambda ()
	     (do ((i 0 
		     (+ 1 i)))
		 ((>= i its-length) 
		  #f)
	       (if (list-pos item (list-ref lst i) pred)
		   (throw 'exit i))))
	   (lambda (key index)
	     index))))

#!

;; this is used to find a specific renderer in a 'to-pack list [of
;; (rendereri columni)] the order of which could change upon
;; time... the renderer could hold the background/foreground color of
;; the row ... Here is a simpler example to test

(first-level-pos-second-level-lookup 5 '((1 2 3) (4 5 6) (7 8 9)) eq?)

!#
