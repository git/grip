;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2011 - 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;; This code needs review and/or be rewritten: it is imported almost 'as
;; is' from 'common', grip's ancestor, started while using guile-1.6, a
;; past where I could not trust the module system, goops, and myself [:)
;; as a schemer].  But I want grip-0.1.0 and kise-0.9.5 ready asap to
;; talk to svannah, gitorious has been murdered.

;;; Code:


(define-module (grip xft)
  #:use-module (ice-9 match)
  #:use-module (ice-9 receive)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 popen)
  #:use-module (oop goops)
  #:use-module (grip module)
  #:use-module (grip utils)
  #:use-module (grip string)
  #:use-module (grip float)
  #:use-module (grip store)

  #:export (xft-store))


(eval-when (expand load eval)
  (re-export-public-interface (grip store)))


(define (xft-get-settings)
  (let* ((cmd "xrdb -query | grep Xft")
         (s (open-input-pipe cmd)))
    (let loop ((result '())
               (line (read-line s)))
      (if (eof-object? line)
          (begin
            (or (zero? (status:exit-val (close-pipe s)))
                (error "subprocess returned non-zero result code" cmd))
            (reverse! result))
          (match (string-tokens line #\tab)
            ((key value)
             (loop (cons (cons (string->symbol (string-drop-right key 1))
                               (string->number value))
                         result)
                   (read-line s))))))))

(define* (xft-make-store #:key (default-dpi 96))
  (let ((store (make <store>))
        (vals (xft-get-settings)))
    (init! store vals #:no-checks #t)
    (set-! store 'scale-factor
           (if (ref store 'Xft.dpi)
               (float-round (exact->inexact (/ (ref store 'Xft.dpi)
                                               default-dpi))
                            2)
               1.0))
    (set-! store 'apply-scale-factor?
           (not (float=? (ref store 'scale-factor) 1.0)))
    store))

(define xft-store #f)

(let ((%xft-store #f))
  (set! xft-store
        (lambda* (#:key (default-dpi 96) (reload #f))
          (or (and (not reload) %xft-store)
              (let ((store (xft-make-store #:default-dpi default-dpi)))
                (set! %xft-store store)
                store)))))
