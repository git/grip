;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2011 - 2018
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.
;;;; A Grip of Really Important Procedures.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful WARRANTY;
;;;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;;;; A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;;; details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(define-module (grip utils)
  #:use-module (ice-9 match)

  #:export (displayln
	    dimfi
	    and-l
            swap!
            ;; undocumented
            abort
	    identities))


;;;
;;; Documented (part of the API)
;;;

(define* (displayln obj #:optional (port #f))
  (if port
      (begin
	(display obj port)
	(newline port))
      (begin
	(display obj)
	(newline))))

(define (dimfi . args)
  ;; if the first obj is a port, we use it.
  (if (port? (car args))
      (let ((p (car args)))
	(display ";; " p)
	(for-each (lambda (obj)
		    (display obj p) (display " " p))
	    (cdr args))
	(display "\n" p))
      (begin
	(display ";; ")
	(for-each (lambda (obj)
		    (display obj) (display " " ))
	    args)
	(display "\n")))
  (car (last-pair args)))

(define (and-l lst)
  (if (null? lst)
      (error "and-l: lst can't be empty" lst)
      (let loop ((lst lst))
        (match lst
          (() #t)
          ((a . rest)
           (if a
               (loop rest)
               #f))))))

(define-syntax swap!
  (syntax-rules ()
    ((swap! ?var1 ?var2)
     (let ((tmp ?var1))
       (set! ?var1 ?var2)
       (set! ?var2 tmp)))))


;;;
;;; Undocumented (not (yet) part of the API)
;;;

(define* (abort what msg port #:key (msg-2 #f) (code -1))
  (display (string-append "ERROR: " what ": " msg) port)
  (newline port)
  (when msg-2
    (display msg-2 port)
    (newline port))
  (exit code))

(define (identities . args)
  args)
