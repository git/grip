@c -*- mode: texinfo; coding: utf-8 -*-
@c This is part of the Grip User Manual.
@c Copyright (C)  2011 - 2016
@c David Pirotte <david at altosw dot be>
@c See the file grip.texi for copying conditions.

@set TITLE Grip User Manual

@set MANUAL-REVISION 1

@set UGUILE http://www.gnu.org/software/guile
@set UGUILE-GOOPS http://www.gnu.org/software/guile/manual/guile.html#GOOPS
@set UGUILE-GOOPS-METHODS https://www.gnu.org/software/guile/manual/html_node/Methods-and-Generic-Functions.html#Methods-and-Generic-Functions
@set UGUILE-RNRS-FLONUMS https://www.gnu.org/software/guile/manual/guile.html#rnrs-arithmetic-flonums
@set UGUILE-ASSOC-LIST https://www.gnu.org/software/guile/manual/guile.html#Association-Lists
@set UGUILE-SCHEME-WRITE https://www.gnu.org/software/guile/manual/html_node/Scheme-Write.html#Scheme-Write
@set UGUILE-REPL-SERVERS https://www.gnu.org/software/guile/manual/html_node/Scheme-Write.html#REPL-Servers


@set UGUILE-LIB http://www.nongnu.org/guile-lib

@set UGUILE-CV http://www.gnu.org/software/guile-cv

@set UGUILE-LAMBDA-STAR https://www.gnu.org/software/guile/manual/html_node/lambda_002a-and-define_002a.html#lambda_002a-and-define_002a

@set UGUILE-LOADPATH http://www.gnu.org/software/guile/manual/guile.html#Load-Paths

@set UGUILE-GNOME http://www.gnu.org/software/guile-gnome
@set UGUILE-CLUTTER http://www.gnu.org/software/guile-gnome/clutter

@set USQLITE http://www.sqlite.org
@set USQLITE3-PCRE https://github.com/oojah/sqlite3-pcre

@set ULATEX http://www.latex-project.org
@set UIWONA http://www.tug.dk/FontCatalogue/iwona/

@set USCHEME http://schemers.org

@set UALTO http://altosw.be

@set UGRIP http://www.nongnu.org/grip/index.html
@set UGRIP-SAVANNAH http://savannah.nongnu.org/projects/grip
@set UGRIP-RELEASES http://download.savannah.gnu.org/releases/grip
@set UGRIP-LATEST http://download.savannah.gnu.org/releases/grip/grip-@value{VERSION}.tar.gz
@set UGRIP-WIKIPEDIA https://en.wikipedia.org/wiki/Grip_(job)

@set UFOLIOT http://www.gnu.org/software/foliot/

@set UPANGO-MARKUP-FORMAT https://developer.gnome.org/pango/stable/PangoMarkupFormat.html
