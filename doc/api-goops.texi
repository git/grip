@c -*- mode: texinfo; coding: utf-8 -*-
@c This is part of the Grip User Manual.
@c Copyright (C)  2011 - 2018
@c David Pirotte <david at altosw dot be>
@c See the file grip.texi for copying conditions.


@node Goops
@section Goops

Grip functionality that extends and/or specializes
@uref{@value{UGUILE-GOOPS}, Goops}:

@lisp
(use-modules (grip goops))
@end lisp

This module import and re-export the public interface of the Guile
module @code{(oop goops)}.  It also imports the Guile module @code{(oop
goops describe)}, which defines six @code{describe} methods that this
module also re-export.

@subheading Syntax and Methods

@ifhtml
@indentedblock
@table @code
@item @ref{define-method*}
@item @ref{class-direct-virtual-slots}
@item @ref{class-virtual-slots}
@item @ref{describe}
@end table
@end indentedblock
@end ifhtml


@subheading Syntax and methods

@anchor{define-method*}
@deffn Syntax define-method* (generic parameter @dots{} @* @
[#:optional vardef…] @* @
[#:key vardef… [#:allow-other-keys]]  @* @
[#:rest var | . var])  @* @
body1 body2 @dots{}

Returns a method.

@code{define-method*} is like @uref{@value{UGUILE-GOOPS-METHODS},
define-method}, except with some extensions to allow optional and
keyword arguments.

Please refer to @uref{@value{UGUILE-LAMBDA-STAR}, lambda* and define*}
for a complete description of how you may specify and refer to
@code{#:optional} and @code{#:key} arguments. For a simple example, see
the definition of @code{get-color} in the module @code{(grip clutter
color)}.
@end deffn


@anchor{class-direct-virtual-slots}
@deffn Method class-direct-virtual-slots (class <class>)

Return a list containing the slot definitions of the direct slots of
@var{class} for which the allocation is @code{#:virtual}.
@end deffn


@anchor{class-virtual-slots}
@deffn Method class-virtual-slots (class <class>)

Return a list containing the slot definitions for all @var{class} slots,
including any slots that are inherited from superclasses, for which the
allocation is @code{#:virtual}.
@end deffn


@anchor{describe}
@deffn Method describe (x <top>)
@deffnx Method describe (x <procedure>)
@deffnx Method describe (x <object>)
@deffnx Method describe (x <class>)
@deffnx Method describe (x <generic>)
@deffnx Method describe (x <method>) . omit-generic

Retrieves and displays @samp{sensible} information upon @var{x}.
@end deffn
