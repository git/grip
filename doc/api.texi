@c -*- mode: texinfo; coding: utf-8 -*-
@c This is part of the Grip User Manual.
@c Copyright (C)  2011 - 2018
@c David Pirotte <david at altosw dot be>
@c See the file grip.texi for copying conditions.


@node API Reference
@chapter API Reference

@c Grip Reference Manual still is a mock-up: any help is more then
@c welcome to improve this situation, thanks!


@include api-module.texi
@include api-goops.texi
@include api-optargs.texi
@include api-list.texi
@include api-string.texi
@include api-queue.texi
@include api-store.texi
@include api-iter.texi
@include api-angle.texi
@include api-float.texi
@include api-server.texi
@include api-xft.texi
@include api-utils.texi
