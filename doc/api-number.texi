@c -*- mode: texinfo; coding: utf-8 -*-
@c This is part of the Grip User Manual.
@c Copyright (C)  2011 - 2016
@c David Pirotte <david at altosw dot be>
@c See the file grip.texi for copying conditions.


@node Numbers
@section Numbers

Grip procedures and variables to process numbers. There is a
@samp{generic} module, which imports all Grip number related modules
and re-export their public interface, which you may load using:

@lisp
(use-modules (grip number))
@end lisp

You may prefer to selectively import Grip number related modules as
needed. Each following subsections describe their respective module
import command.

@menu
* Angles::
* Floats::
@end menu

@include api-number-angle.texi
@include api-number-float.texi
