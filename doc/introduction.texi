@c -*- mode: texinfo; coding: utf-8 -*-
@c This is part of the Grip User Manual.
@c Copyright (C)  2011 - 2018
@c David Pirotte <david at altosw dot be>
@c See the file grip.texi for copying conditions.


@node Introduction
@chapter Introduction

@menu
* About Grip::
* Objective::
* The name::
* Savannah::
@c * What else::
* Obtaining and Installing Grip::
* Contact::
* Reporting Bugs::
@end menu


@c @unnumberedsec About Grip
@node About Grip
@section About Grip


@uref{@value{UGRIP}, Grip}, a Grip of Really Important Procedures, is a
@uref{@value{UGUILE}, Guile} Scheme toolbox currently composed of
@emph{Grip} itself, @emph{Grip-SQLite}, @emph{Grip-Gnome} and
@emph{Grip-Clutter}.

@quotation
@strong{Notes:}

@uref{@value{UGRIP}, Grip} will try to install all its components, but
will only do so if it can effectively install Grip (core) and can
statisfy the component specific requirement(s): if you are only
interested by Grip (core), then you only need @uref{@value{UGUILE},
Guile}, and you may safely ignore all other dependencies.

@emph{Grip (core)}, @emph{Grip-SQLite} and @emph{Grip-Gnome} are being
rewritten and documented, you will spot that by yourself in the code if
you visit it, and read the documentation.

Until this process is complete, the modules you can safely use and rely
on are the @emph{Grip (core)} modules that are documented, as well as
all @emph{Grip-Clutter} modules and examples.
@end quotation

You are welcome to try and use @uref{@value{UGRIP}, Grip}, and, keeping
the above in mind, help us to get it better - reviewing its interface
design, the source code, its tests and documentation.

New features are also welcome! Though in order to acheive stability, to
the best we can, we should, with all due respect and kindness, be
'nit-picky' with each other, and only include those that reach a
consensus, first with respect to their inclusion per se, then in terms
of interface design, implementation, tests and documentation.


@node Objective
@section Objective

@uref{@value{UGRIP}, Grip} started as a personnal toolbox, aggregating
modules I wrote to support other projects and clearly hosting 'reusable'
functionalities. Now that I started to revisit Grip's core modules [May
2018], restructuring and rewritting them, with a proper interface
design, implementation, documentation and test-suite, now is a good time
to share this work and invite other guilers to either use Grip, or even
beter contribute to it.

The idea is similar to the one expressed in @uref{@value{UGUILE-LIB},
Guile-Lib}, that is, a place for people to collaborate to the
development of a common library, though less intimidating maybe and,
unlike Guile-Lib, Grip will tolerate, under strict conditions, some C
code: this is notably to support and boost @uref{@value{UGUILE-LIB},
Guile-CV} floating point operations, at least till @uref{@value{UGUILE},
Guile} has an AOT compiler, able to offer similar performance results
for fixed size floating points and integer operations.

I should mention that I started Grip far before I became one of the
Guile-Lib co-maintainer, and that the intention is not to compete with
it, but rather, to offer sort of a code staging for it, where people can
experiment new functionalities with a bit more flexibility. With time,
the well designed, stable, documented and tested pure scheme interfaces
could be moved to Guile-Lib.

@node The name
@section The name

The term @strong{Grip} is from the early era of the circus. See the
following @uref{@value{UGRIP-WIKIPEDIA}, Wikipedia}, page for a full
description, where you'll read the following extract:

@quotation
... From there it was used in vaudeville and then in today's film sound
stages and sets. Some have suggested the name comes from the 1930s–40s
slang term for a @strong{Tool Bag} or @strong{Grip} that these
technicians use to carry their tools...
@end quotation


@node Savannah
@section Savannah

@uref{@value{UGRIP}, Grip} also has a @uref{@value{UGRIP-SAVANNAH},
Savannah} (non GNU) project page.


@c @node What else
@c @section What else

@c Grip is written in @uref{@value{UGUILE}, guile}, an interpreter and
@c compiler for the @uref{@value{USCHEME}, scheme} programming language.
@c It uses @uref{@value{UGUILE-GNOME}, guile-gnome}, <fixme guile-clutter>,
@c @uref{@value{USQLITE}, SQLite}.


@node Obtaining and Installing Grip
@section Obtaining and Installing Grip

@emph{Grip} can be obtained from the following
@uref{@value{UGRIP-RELEASES}, archive} site.  The file will be named
@emph{grip-version.tar.gz}. The current version is @value{VERSION}, so
the file you should grab is:

@tie{}@tie{}@tie{}@tie{}@uref{@value{UGRIP-LATEST}, grip-@value{VERSION}.tar.gz}

@menu
* Dependencies::
* Quickstart::
@end menu

@node Dependencies
@subsection Dependencies

Grip will look for the following dependencies and will try to install
all its components, but will only do so if it can effectively install
Grip core and statisfy the component specific requirement(s).

If you are only interested by Grip (core), then you only need
@uref{@value{UGUILE}, Guile}, and you may safely ignore all
other dependencies.

@strong{Grip}

@itemize @bullet
@item
Autoconf >= 2.69
@item
Automake >= 1.14
@item
@uref{@value{UGUILE}, Guile-2.0} >= 2.0.14 or Guile-2.2
@item
@uref{@value{UGUILE-LIB}, Guile-Lib} >= 0.2.5@*

This is a @samp{soft} dependency: Guile-Lib is required to run the
test-suite, which is recommended but not mandatory.
@end itemize

@strong{Grip-SQLite}

@itemize @bullet
@item 
@uref{@value{USQLITE}, SQLite} >= 3.7
@end itemize

@strong{Grip-Gnome}

@itemize @bullet
@item
@uref{@value{UGUILE-GNOME}, Guile-Gnome} >= 2.16.5, the following
wrappers:

  @indentedblock
  Glib, Gobject, Gtk, libglade
  @end indentedblock
@end itemize

@strong{Grip-Clutter}

@itemize @bullet
@item
@uref{@value{UGUILE-CLUTTER}, Guile-Clutter} >= 1.12.2.1
@end itemize


@node Quickstart
@subsection Quickstart

Assuming you have satisfied the dependencies, open a terminal and
proceed with the following steps:

@example
cd <download-path>
tar zxf grip-@value{VERSION}.tar.gz
cd grip-@value{VERSION}
./configure [--prefix=/your/prefix] [--with-guile-site=yes]
make
make install
@end example

Grip comes with a tests suite, which you may run (recommended) using:

@example
make check
@end example

Happy Grip!


@strong{Notes:}

@enumerate
@item
The @code{default} and @code{--prefix} installation locations for source
modules and compiled files (in the absence of
@code{--with-guile-site=yes}) are:

@example
$(datadir)/grip
$(libdir)/grip/guile/$(GUILE_EFFECTIVE_VERSION)/site-ccache
@end example

If you pass @code{--with-guile-site=yes}, these becomes:

@example
the Guile site directory
the Guile site-ccache directory
@end example

@item
The configure step reports these locations as the content of the
@code{sitedir} and @code{siteccachedir} variables, respectivelly the
source modules and compiled files install locations.

After installation, you may consult these variables using pkg-config:

@example
pkg-config grip-1.0 --variable=sitedir
pkg-config grip-1.0 --variable=siteccachedir
@end example

@item
To install Grip, you must have write permissions to the default or
$prefix dir and its subdirs, as well as to both Guile's site and
site-ccache directories if @code{--with-guile-site=yes} was passed.
@ifhtml
@*@*
@end ifhtml

@item
Like for any other GNU Tool Chain compatible software, you may install
the documentation locally using @code{make install-html} [or @code{make
install-pdf} ...].
@ifhtml
@*@*
@end ifhtml

@item
Last but not least, Grip comes with a test-suite, which we recommend you
to run (especially before @ref{Reporting Bugs})):

@example
make check
@end example

@end enumerate


@node Contact
@section Contact

@subheading Mailing lists

Grip uses Guile's mailing lists:

@indentedblock
@email{guile-user@@gnu.org} is for general user help and discussion;

@email{guile-devel@@gnu.org} is used to discuss most aspects of Grip,
including development and enhancement requests.
@end indentedblock

Please use 'Grip: ' to preceed the subject line of Grip related emails,
thanks!

@subheading IRC

Most of the time you can find me on irc, channel @code{#guile},
@code{#guix} and @code{#scheme} on @code{irc.freenode.net},
@code{#clutter} and @code{#introspection} on @code{irc.gnome.org}, under
the nickname @code{daviid}.


@node Reporting Bugs
@section Reporting Bugs

Grip does not have its own bug reports mailing list yet. Please send
your bug reports to one of the @uref{@value{UGUILE}, Guile's} mailing
listts:

@indentedblock
@email{guile-user@@gnu.org} @*
@email{guile-devel@@gnu.org}
@end indentedblock

Please use 'Grip - bug report: ' to preceed the subject line of Grip bug
reports related emails, thanks!
